# Exoplanet Exploration

In this project we analyze the dataset from [Exoplanets](https://www.kaggle.com/nasa/kepler-exoplanet-search-results) to develop two separate rediction models. 

The first model evaluated is the Support Vector Machine with a linear kernel with a performance shown by the confusion matrix:

                precision    recall  f1-score   support

     CANDIDATE       0.76      0.60      0.67       562
     CONFIRMED       0.68      0.84      0.75       573
FALSE POSITIVE       0.98      0.98      0.98      1256

      accuracy                           0.85      2391
     macro avg       0.81      0.80      0.80      2391
  weighted avg       0.86      0.85      0.85      2391
  
  
 Also, in an attempt to obtain the best possible values for the SVC model, the GridSearch function was evaluated. Unfortunately the GridSearch algorithm is a very expensive operation and the operation was canceled.
 
 * 48 Core machine running GridSearch
 ![GridSearch](data/grid-fit-48-cores.png)
 
 
 
The second model evaluated was a Deep Neural Network

Model: "sequential"


|Layer (type)     |       Output Shape        |        Param #|
|-----------------|:-------------------------:|--------------:|
|dense (Dense)    |           (None, 40)      |         1640  |
|dense_1 (Dense)  |           (None, 40)      |         1640  |
|dense_2 (Dense)  |           (None, 3)       |         123   |

Total params: 3,403
Trainable params: 3,403
Non-trainable params: 0


75/75 - 0s - loss: 0.2627 - accuracy: 0.8934
Loss: 0.262656033039093, Accuracy: 0.8933500647544861


Comparing both clasification models evaluated, it appears like the deep neural network has slightly better performance than the SVC model.


It is important to note that a total of 40 features were used to generate the models and the column used as a response vectore was the `koi_disposition`.
In addition, the column `koi_score` was left out because it appears to be an original estimation to classify exoplanets.
